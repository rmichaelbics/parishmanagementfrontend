import React, { useState } from "react";
import { Image, Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
// import '../../assets/js/main';

export const Login = () => {

  return (


        
        <div className="container">
		<Image className="wave" src="../../assets/images/wave.png"/>
		<div className="img">
			<Image srcSet="../../assets/images/bg.svg"/>
		</div>
		<div className="login-content">
			<form>
				<Image srcSet="../../assets/images/avatar.svg"/>
				<h2 className="title">Welcome</h2>
           		<div className="input-div one">
           		   <div className="i">
           		   		<i className="fas fa-user"></i>
           		   </div>
           		   <div className="div">
           		   		<h5>Username</h5>
           		   		<input type="text" className="input"/>
           		   </div>
           		</div>
           		<div className="input-div pass">
           		   <div className="i"> 
           		    	<i className="fas fa-lock"></i>
           		   </div>
           		   <div className="div">
           		    	<h5>Password</h5>
           		    	<input type="password" className="input"/>
            	   </div>
            	</div>
            	<a href="#">Forgot Password?</a>
            	<input type="submit" className="btn" value="Login"/>
            </form>
        </div>
    </div> 
    )
}