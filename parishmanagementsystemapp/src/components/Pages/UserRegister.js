import React, { useState } from "react";
import { Row,Col, FormControl, FormLabel, FormGroup, Form } from 'react-bootstrap';

export const UserRegister = () =>{
   
        return(
            <div className="container-fluid bg-3 text-center Login" >
            <form>
                <div className="container">
                    <div className="">
                        <Form.Label>Firstname</Form.Label>
                    </div>
                    <div>
                        <FormControl type="text" id="txtFirstname" className="form-control" placeholder="Firstname">

                        </FormControl>
                    </div>
                    <div className="">
                        <Form.Label>Lastname</Form.Label>
                    </div>
                    <div>
                        <FormControl type="text" id="txtLastname" className="form-control" placeholder="Lastname">

                        </FormControl>
                    </div>
                </div>
            </form>
            </div>

           
        )
}