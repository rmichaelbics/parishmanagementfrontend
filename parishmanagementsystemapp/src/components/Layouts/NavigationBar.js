import React from 'react';
import { Nav,Navbar, NavbarBrand } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`
.navbar {
    background-color: #ffc107;
    border:2;
    margin:2;
}

.navbar-nav .nav-link
{

}
.navbar-brand{
    color: #f8f9fa;
    font-bold:10px;

&:hover {
    color:#white;
    }
} `;

export const NavigationBar =() => (
    <Styles>
        <Navbar expand="lg">
            <Navbar.Brand href="#"><h5>St Joseph Chruch, Kathanpallam</h5></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" ></Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
                <Nav.Item><Nav.Link href="/">Home</Nav.Link> </Nav.Item>
                <Nav.Item><Nav.Link href="/about">About</Nav.Link> </Nav.Item>
                <Nav.Item><Nav.Link href="/contact">Contact</Nav.Link> </Nav.Item>
                <Nav.Item><Nav.Link href="/login">Login</Nav.Link> </Nav.Item>
                <Nav.Item><Nav.Link href="/userregister" className="btn btn-warning-outline">Sign Up</Nav.Link> </Nav.Item>
                {/* <Nav.Item><Nav.Link href="/NoMatch">No Match</Nav.Link> </Nav.Item> */}
            </Nav>
            </Navbar.Collapse>
        </Navbar>
    </Styles>
)
