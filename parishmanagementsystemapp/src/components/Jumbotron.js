import React from 'react';
import { Jumbotron as Jumbo, Container } from 'react-bootstrap';
import styled from 'styled-components';
import natureImage from '../assets/image1.jpeg';

const Styles = styled.div`
.Jumbo {
    background: url(${natureImage}) no-repeat fixed bottom;
    background-size: cover;
    color: #efefef;
    height: 200px;
    position: relative;
    z-index:-2;
}

.overlay {
    background-color: #000;
    opacity: 0.6;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: -1;

}

`;
export const Jumbotron = () => (
    <Styles>
        <Jumbo fluid className ="Jumbo">
            <div className="overlay"></div>
        </Jumbo>
    </Styles>
)
