import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Home } from './components/Home';
import { About } from './components/About';
import { Contact } from './components/Contact';
import { NoMatch } from './components/NoMatch';
import { Layout } from './components/Layouts/Layout';
import { NavigationBar } from './components/Layouts/NavigationBar';
import { Login } from './components/Pages/Login';
// import {Jumbotron} from './components/Jumbotron';
import {UserRegister} from './components/Pages/UserRegister';

class App extends Component {

  render(){
    return (
      <React.Fragment>
        <NavigationBar></NavigationBar>
        {/* <Jumbotron/> */}
        <Layout></Layout>
        <Router>
          <Switch>
            <Route exact path="/"  component={Home}></Route>
            <Route path="/about" component={About}></Route>
            <Route path="/contact" component={Contact}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/userregister" component={UserRegister}></Route>
            <Route component={NoMatch}></Route>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
